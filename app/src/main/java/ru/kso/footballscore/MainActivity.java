package ru.kso.footballscore;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private Integer scoreOfFirstTeam = 0;
    private Integer scoreOfSecondTeam = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("scoreOfFirstTeam", scoreOfFirstTeam);
        outState.putInt("scoreOfSecondTeam", scoreOfSecondTeam);
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState.containsKey("scoreOfFirstTeam") &&
                savedInstanceState.containsKey("scoreOfSecondTeam")) {
            scoreOfFirstTeam = savedInstanceState.getInt("scoreOfFirstTeam");
            scoreOfSecondTeam = savedInstanceState.getInt("scoreOfSecondTeam");
            resetFirstScore();
            resetSecondScore();
        }
    }

    private void resetFirstScore() {
        TextView scoreFirst = findViewById(R.id.scoreFirstTeam);
        scoreFirst.setText(String.valueOf(scoreOfFirstTeam));
    }

    private void resetSecondScore() {
        TextView scoreSecond = findViewById(R.id.scoreSecondTeam);
        scoreSecond.setText(String.valueOf(scoreOfSecondTeam));
    }

    public void addPointToFirst(View view) {
        scoreOfFirstTeam++;
        resetFirstScore();
    }

    public void addPointToSecond(View view) {
        scoreOfSecondTeam++;
        resetSecondScore();
    }

    public void clearScore(View view) {
        scoreOfFirstTeam = 0;
        scoreOfSecondTeam = 0;
        resetFirstScore();
        resetSecondScore();
    }
}
